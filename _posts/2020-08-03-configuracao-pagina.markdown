---
layout: post
title:  "Ciência de Dados em cenários de pandemia - visão geral"
date:   2020-08-03 5:32:14 -0300
categories: info update
---

O foco de aplicação dos trabalhos práticos da disciplina é em cenários de pandemia. 
Obviamente este é um assunto importante neste momento. 
Mas além disso, esta é uma oportunidade para tratarmos de um tema complexo, que envolve diversos aspectos da ciência de dados e enfatiza a importância do conhecimento multidisciplinar. 
Durante a disciplina, vamos discutir questões epidemiológicas, sociais, econômicas, etc. 

Além das aulas teóricas e discussões práticas, teremos a participação de especialistas de diversas áreas. Os especialistas organizarão palestras ou mini-aulas mostrando suas pesquisas e explicando conceitos importantes.

Embora o foco da disciplina seja em cenários de pandemia, abordaremos também outros tópicos, como tecnologias e o mercado de trabalho para cientistas de dados.

Saiba mais sobre a disciplina nos links do menu do site.