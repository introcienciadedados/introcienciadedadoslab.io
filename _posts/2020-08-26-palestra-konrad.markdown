---
layout: post
title:  "Palestra confirmada: Konrad Ilczuk"
date:   2020-08-26 12:32:14 -0300
categories: info palestra
---

Na aula do dia 24/09 teremos a participação do Konrad Ilczuk com a palestra "Horizontes ampliados - por que a interdisciplinaridade importa".

![Konrad Ilczuk](/assets/talk_konrad.jpeg)

Mini biografia: Konrad Ilczuk é cientista de dados com experiência diversificada - tanto geograficamente (projetos na China, Suécia, Espanha, Brasil e Polônia) quanto de indústrias (educacional, manufatura, automotiva, financeira e retail). Formou-se com mestrado no Royal Institute of Technology em Estocolmo, que incluiu um ano de intercâmbio no Brasil, na Unicamp. Hoje em dia trabalha como Principal Data Scientist e Management Consultant para uma empresa de telecomunicações na Suécia.