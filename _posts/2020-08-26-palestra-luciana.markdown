---
layout: post
title:  "Palestra confirmada: Luciana Drumond"
date:   2020-08-26 12:33:14 -0300
categories: info palestra
---

Na aula do dia 29/09 teremos a participação da Luciana Drumond com a palestra "Fontes de dados demográficos e socioeconômicos em interface com a COVID".

![Luciana Drumond](/assets/talk_lumel.jpeg)

Mini biografia: Formada em Psicologia, mestrado e doutorado em Sociologia pela UFMG com foco em desigualdades e estratificação social. Atualmente colaboradora da Rede Análise COVID-19.