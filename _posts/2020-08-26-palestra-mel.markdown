---
layout: post
title:  "Palestra confirmada: Mel Veneroso"
date:   2020-08-26 12:34:14 -0300
categories: info palestra
---

Na aula do dia 29/09 teremos a participação da Mel Veneroso (no monitor na foto abaixo!) com a palestra "A importância da política pública baseada em evidências, as principais fontes de dados sociais no Brasil e os principais indicadores utilizados".

![Mel Veneroso, no monitor](/assets/talk_lumel.jpeg)

Mini biografia: Socióloga pela UFMG. Pesquisadora e consultora em avaliação e monitoramento de políticas e programas sociais, e pesquisas de mercado. Já prestou consultoria para o estado de Minas Gerais, Ministério do Desenvolvimento Social (MDS), organismos internacionais (PNUD, BID e UNFPA) e diversas instituições privadas e bancos, como Fundação Dom Cabral, Banco Central do Brasil e Caixa Econômica Federal. Suas principais áreas do conhecimento são Estratificação e Desigualdade Social, com foco em gênero. Atualmente, colabora com a Rede Análise Covid-19.