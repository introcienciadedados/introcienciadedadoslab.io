---
layout: post
title:  "Palestra confirmada: Camila Genaro Estevam"
date:   2020-09-01 12:34:14 -0300
categories: info palestra
---

Na aula do dia 29/09 teremos a participação da Camila Genaro Estevam com a palestra "Análise de dados no combate a Covid: do rastreamento de contatos até o georreferenciamento".

![Camila Genaro Estevam](/assets/camila.jpg)

Mini biografia: Bióloga pela UNESP e mestranda em Epidemiologia pelo programa de Saúde Coletiva na UNICAMP. Possui experiência em análises espaciais aplicadas a estudos em saúde.Nos últimos 5 anos tem trabalhado com arboviroses a partir de uma perspectiva espacial. Membro do Observatório COVID-BR onde atua com análise de dados descritivos voltado para tomada de decisões de secretarias de saúde e implementação de rastreamento de contato.