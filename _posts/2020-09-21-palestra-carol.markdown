---
layout: post
title:  "Palestra confirmada: Caroline Franco"
date:   2020-09-21 1:34:14 -0300
categories: info palestra
---

Na aula do dia 24/09 teremos a participação da Caroline Franco com a palestra "Contextualizando a modelagem matemática de COVID-19 no Brasil".

![Caroline Franco](/assets/talk_carol.png)

Mini biografia: Física, Mestre (IFSC - USP) e doutoranda em Física Teórica (IFT-UNESP) com experiência em modelagem matemática de sistemas biológicos, focando principalmente na dinâmica de transmissão de doenças infecciosas.
Durante o doutorado tem trabalhado em modelagem e análise de dados de casos de dengue, malária e, mais recentemente, COVID-19 no Brasil.
Desde o início da pandemia, colabora com o Observatório COVID-19 BR e o CoMo Consortium na construção de modelos matemáticos que simulam o efeito de diferentes cenários de intervenções não-farmacológicas na mitigação da epidemia.