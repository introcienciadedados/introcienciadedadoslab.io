---
layout: post
title:  "Palestra confirmada: Caio Jardim-Sousa"
date:   2020-09-21 1:34:14 -0300
categories: info palestra
---

Na aula do dia 24/09 teremos a participação do Caio Jardim-Sousa com a palestra "Estatística pública e dados governamentais: introdução conceitual para cientistas e analistas de dados".

![Caio Jardim-Sousa](/assets/talk_caio.png)

Mini biografia: Sociólogo com aperfeiçoamento em métodos quantitativos e estudos em raça, gênero e sexualidade na UFMG. Pesquisador do Núcleo Afro do CEBRAP e da Rede Análise COVID-19. Consultor de pesquisas quantitativas em inteligência de mercado e produto, socioeconomia, opinião pública, experiência do usuário e economia comportamental.