---
layout: post
title:  "Palestra confirmada: Luiz Ledo"
date:   2020-11-10 1:34:14 -0300
categories: info palestra
---

Na aula do dia 01/12 teremos a participação do Luiz Ledo com a palestra "Introdução a Geoestatística".

![Luiz Ledo](/assets/talk_ledo.jpg)

Mini biografia: Doutor em  Engenharia Elétrica pela Universidade Tecnológica Federal do Paraná - UTFPR-PR (2017) com  período de Doutorado sanduíche na Universidade do Algarve-UALG-Portugal(2015), possui mestrado em  Estatística pela Universidade Federal do Rio de Janeiro- UFRJ (2008) e graduou-se  em Estatística pela Universidade Federal do Paraná  - UFPR (2004). É professor  da Universidade Tecnológica Federal do Paraná - UTFPR, onde atua desde 2012 como professor. Atualmente é professor  pesquisador no Programa de Pós-Graduação em Engenharia Elétrica  e Informática Industrial (CPGEI) e no departamento de Estatística (DAEST). Foi estatístico da Secretaria de Segurança Pública do estado do Paraná (2005-2006). Tem interesse em metodologias estatísticas com ênfase em Inferência Bayesiana.