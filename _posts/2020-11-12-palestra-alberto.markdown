---
layout: post
title:  "Palestra confirmada: Alberto Aleta"
date:   2020-11-12 1:34:14 -0300
categories: info palestra
---

Na aula do dia 19/11 (às 16h) teremos a participação do Alberto Aleta com a palestra "COVID-19 & Networks: a quest for data".

![Alberto Aleta](/assets/talk_alberto.png)

Mini biografia: Alberto Aleta is a Postdoctoral Researcher at ISI Foundation in Turin, Italy. Alberto obtained his Ph.D. in theoretical physics at the University of Zaragoza, Spain. His research interests lie in the broad area of complex systems, network science, and data science. He has collaborated in projects of diverse disciplines from epidemiology to online videogames, but during the current pandemic his work has mostly focused on studying the spreading of COVID-19.

Mais detalhes sobre o trabalho do Alberto no seu [site pessoal](https://aaleta.github.io/).
