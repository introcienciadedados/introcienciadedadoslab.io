---
layout: post
title:  "Palestra confirmada: Luciana Martha Silveira"
date:   2020-11-07 1:34:14 -0300
categories: info palestra
---

Na aula do dia 03/12 teremos a participação da Luciana Martha Silveira com a palestra "Paletas Possíveis: Introdução à Teoria da Cor".

![Luciana Martha Silveira](/assets/talk_luciana.jpg)

Mini biografia: Doutora em Comunicação e Semiótica pela Pontifícia Universidade Católica de São Paulo  - PUC-SP (2002), possui mestrado em Multimeios pela Universidade Estadual de Campinas - UNICAMP (1994) e graduou-se em Educação Artística pela Universidade Estadual de Campinas - UNICAMP (1989). É professora associada da Universidade Tecnológica Federal do Paraná - UTFPR, onde atua desde 1998 como professora e pesquisadora no Programa de Pós-Graduação em Tecnologia e Sociedade (PPGTE) e no departamento de Design (DADIN). Foi coordenadora do PPGTE no ano de 2009 e orienta trabalhos dentro da Linha de Pesquisa Mediações e Culturas. É membro do conselho editorial da Revista de Artes Visuais Art&Sensorium. Suas pesquisas privilegiam as relações entre Arte e Tecnologia, com ênfase nas implicações dessas relações para a área CTS. Entre 2009 e 2010, realizou estágio pós-doutoral como professora visitante na Universidade de Michigan, EUA, de onde resultou o livro de sua autoria ¨Introdução à Teoria da Cor¨, publicado pela editora UTFPR, 2011 (2a. edição 2015). É artista visual, sendo seu trabalho artístico voltado para a pintura, participando de exposições individuais e coletivas.