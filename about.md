---
layout: page
title: Sobre a disciplina
permalink: /about/
---



## Professores

- Luiz Celso Gomes Jr (UTFPR) - [Página pessoal](http://dainf.ct.utfpr.edu.br/~gomesjr/) - [Twitter](https://twitter.com/luizcelso)


## Objetivos

Ensinar os principais conceitos de Ciência de Dados com aplicação prática em problemas atuais. Uma ênfase especial será dada em:



*   Complexidade do domínio e importância de inferências bem fundamentadas
*   Multidisciplinaridade
*   Diversidade de modelos




## Conteúdo programático


*   Introdução e Contextualização: Motivação, exemplos, história, contexto e visão geral de tecnologias relacionadas.
*   Obtenção e Análise de Dados: Técnicas para a obtenção, limpeza e processamento de dados.
*   Tópicos em modelagem estatística: conceitos básicos, projeto de experimentos e interpretação de resultados.
*   Visualização de Dados: Principais técnicas para a apresentação visual de dados e resultados. 
*   Tópicos em aprendizado de máquina: aprendizado supervisionado e aprendizado não supervisionado.
*   Manipulação de Dados em Grande Escala: Limitações do Modelo Relacional, Bancos de Dados Paralelos e Distribuídos, Bancos NoSQL, MapReduce, Modelos de Armazenamento e Linguagens de Processamento Distribuído
*   Modelagem de alta dimensionalidade: Ciência de Redes, Propriedades das Redes Complexas, Tópicos em processamento de linguagem natural
*   Ética em Ciência de Dados



## Modalidade, Metodologia e Infraestrutura

### Aulas

As aulas se desenvolverão no ambiente virtual Google Classroom de forma assíncrona, mas alunos terão a opção de participar de conferências de debate e atendimento online.



*   Vídeos serão postados online no início de cada semana.
*   Sessões para debates sobre o tema da semana no horário de aula (programação divulgada no início do curso).

Também haverá oportunidade de se tirar dúvidas e participar de debates de forma assíncrona no Discord.


### Google Classroom

Avisos, notas de aula, conteúdos de leitura suplementar, listas de exercício, resultados de avaliações e todas as atividades de participação serão compartilhadas no ambiente Google Classroom. 



### Atividades Avaliativas

Em geral a avaliação é baseada em um trabalho de análise feito em grupo ao longo do período. Pequenas variações podem ocorrer dependendo do período, mas sempre comunicadas no início do curso.

### Ofertas anteriores

- [2020/2](/about20202)