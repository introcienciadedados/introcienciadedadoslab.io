---
layout: page
title: Perguntas frequentes
permalink: /faq/
---


## Não sou aluno de computação. Posso fazer a disciplina?

Sim! Multidisciplinaridade é muito importante em Ciência de Dados

## É preciso experiência em programação?

Não é preciso, mas é importante se familiarizar com os conceitos e aprender os procedimentos mais comuns.

