---
layout: page
title: Recursos
permalink: /resources/
---

# Material didático

- [Tutoriais de Python e Pandas](https://gitlab.com/introcienciadedados/tutoriais)

# Projetos relacionados

Projetos com participação dos professores da disciplina. Podem ser usados como fontes de inspiração e dados.

- [Ciência de Dados por uma Causa](https://dadosecausas.org/)
- [COVID-19 Como está meu país?](https://coronavirus.dadosecausas.org/)
- [Observatório COVID-19 BR](https://covid19br.github.io/)

# Fontes de dados

Algumas fontes recomendadas de dados.

- [Brasil.io](https://brasil.io/dataset/covid19/caso)
- [Our World in Data](https://ourworldindata.org/)
- [The World Bank](http://datatopics.worldbank.org/universal-health-coverage/covid19/)
- [PNAD-COVID19 IBGE](https://covid19.ibge.gov.br/pnad-covid/)

# Ferramentas

Principais ferramentas discutidas na disciplina (alunos são encorajados a explorar ferramentas complementares se puderem).

- [pandas (python) - Biblioteca de manipulação de dados](https://pandas.pydata.org/)
- [Jupyter (python) - Ambiente de desenvolvimento e documentação](https://jupyter.org)
- [RStudio (R) - Ambiente de desenvolvimento](https://rstudio.com/)
- [Git - Controle de versão](https://git-scm.com/)
- [SmartGit - Interface multiplataforma para o Git](https://www.syntevo.com/smartgit/download/)
